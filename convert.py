import pandas as pd
import datetime

# These must be set before each run.
xlsxfile = 'Sherwood2018.xlsx'
runfile = 'run.csv'
peoplefile = 'people.csv'

#missingpeoplekey = 10000
#runkey = 1
#missingpeoplekey = 11000
#runkey = 1100
missingpeoplekey = 12000
runkey = 11000

# Given time in string Min.Secs, convert to integer seconds.
def GetTime(TimeStr):

    #print('TimeStr: ' + TimeStr)

    TimeStr = ''.join(c for c in TimeStr if c.isdigit() or c == '.')     # Only allow digits . digits

    if len(TimeStr) >= 1 and len(TimeStr) <= 2 and TimeStr.find('.') == -1:
        # eg 22 or 6, add a dot .00
        TimeStr += '.00'

    Time = TimeStr.split('.')
    RunTime = -1

    if len(Time) == 2:
        MinsStr = str(Time[0])
        SecsStr = str(Time[1])
        if len(SecsStr) == 1:
            # 12.30 is read as 12.3 and hence 3 seconds! Convert to 30 seconds.
            SecsStr += '0'
        Mins = int(MinsStr)
        Secs = int(SecsStr)
        RunTime = Mins * 60 + Secs

    #print(RunTime)
    return RunTime


def DetermineCategory(TabName):

    over60 = 'N'

    if TabName.find("2 Km Female") >= 0:
        distance = 2
        sex = 'F'

    elif TabName.find("4 Km Female") >= 0:
        distance = 4
        sex = 'F'

    elif TabName.find("6 Km Female") >= 0:
        distance = 6
        sex = 'F'

    elif TabName.find("2 Km Male") >= 0:
        distance = 2
        sex = 'M'

    elif TabName.find("4 Km Male") >= 0:
        distance = 4
        sex = 'M'

    elif TabName.find("6 Km Male") >= 0:
        distance = 6
        sex = 'M'

    elif TabName.find("60 Years & Over - 6Km") >= 0:
        distance = 6
        sex = 'U'
        over60 = 'Y'

    else:
        distance = -1
        sex = 'U'
        over60 = 'U'

    return distance, sex, over60

def GenerateOutput(outrunfile, outpeoplefile, TabName, Count, missingpeoplekey, runkey):

    # category is 2 Km Female, ... 60 Years & Over - 6Km.

    print('Processing ' + TabName + '...')

    # Spreasheet received from Sherwood recorders.
    df = pd.read_excel(xlsxfile, TabName)

    distance, sex, over60 = DetermineCategory(TabName)

    Titles = []

    for title,content in df.iteritems():
        title = str(title).replace(' 00:00:00', ' ').strip()
        Titles.append(title)

    rows = 0
    i = 0

    for name, row in df.iterrows():
        cols = 0
        OutStr1 = ""
        OutStr2 = ""
        for r in row:
            StrVal = str(r)

            if cols == 0:                   # Ref - not used.
                i = 1

            elif cols == 1:                 # RunnerId
                if StrVal == 'nan':
                    RunnerId = missingpeoplekey
                    print('Used missingpeoplekey: ' + str(missingpeoplekey))
                    missingpeoplekey += 1
                else:
                    RunnerId = int(float(StrVal))

            elif cols == 2:                 # First Name, Last Name.
                FullName = StrVal.split(',')
                if len(FullName) == 2:
                    # Name has to be LastName comma FirstName.
                    LastName = str(FullName[0]).strip()
                    FirstName = str(FullName[1]).strip()
                    outpeoplerecord = str(RunnerId) + ',' + FirstName + ',' + LastName + ',' + '-1' + ',' + sex + ',' + over60 + ',Email,Address'
                    outpeoplefile.write(outpeoplerecord + '\n')
                    print(outpeoplerecord)
                    Count += 1
                else:
                    if StrVal == 'nan':
                        print('Count: ' + str(Count))
                        return Count, missingpeoplekey, runkey

                    print('\tERROR! Invalid name: ' + StrVal)

            elif str(r) != 'nan':
                Heading = str(Titles[cols])
                if RunnerId == 2888:
                    print('===============================' + Heading)
                if len(Heading) == 10:
                    DateParts = Heading.split('-')
                    if len(DateParts[0]) == 4 and len(DateParts[1]) == 2 and len(DateParts[2]) == 2:
                        # Heading looks like a date.
                        RunTime = GetTime(str(r))
                        if RunTime != -1:
                            if RunnerId == 2888:
                                print('===================2888: ' + str(r))
                            outrunrecord = str(runkey) + ',' + Heading + ',' + str(RunnerId) + ',' + str(distance) + ',' + str(RunTime) + ',' + 'N'
                            if RunnerId == 2888:
                                print(outrunrecord)

                            runkey += 1
                            outrunfile.write(outrunrecord + '\n')

            cols += 1

        rows += 1

    print('Count: ' + str(Count))
    return Count, missingpeoplekey, runkey

# Main program.

# Generate run.csv and people.csv to be uploaded into GCP Bucket.
# run.csv:
#     id,Date,Runnerid,2|4|6,TimeInSecs,SpitTheDummy
outrunfile = open(runfile, "a+")

outpeoplefile = open(peoplefile, "a+")

count = 0
count, missingpeoplekey, runkey = GenerateOutput(outrunfile, outpeoplefile, '2 Km Female', count, missingpeoplekey, runkey)

count, missingpeoplekey, runkey = GenerateOutput(outrunfile, outpeoplefile, '4 Km Female', count, missingpeoplekey, runkey)

count, missingpeoplekey, runkey = GenerateOutput(outrunfile, outpeoplefile, '6 Km Female', count, missingpeoplekey, runkey)

count, missingpeoplekey, runkey = GenerateOutput(outrunfile, outpeoplefile, '2 Km Male', count, missingpeoplekey, runkey)

count, missingpeoplekey, runkey = GenerateOutput(outrunfile, outpeoplefile, '4 Km Male', count, missingpeoplekey, runkey)

count, missingpeoplekey, runkey = GenerateOutput(outrunfile, outpeoplefile, '6 Km Male', count, missingpeoplekey, runkey)

count, missingpeoplekey, runkey = GenerateOutput(outrunfile, outpeoplefile, '60 Years & Over - 6Km', count, missingpeoplekey, runkey)

print('Total of ' + str(count) + ' runners.')
print('Final missingpeoplekey: ' + str(missingpeoplekey))
print('Final runkey: ' + str(runkey))

outrunfile.close()
outpeoplefile.close()
